# My Second Try With CSharp

🧰 : `Microsoft .Net Framework 4.8.04084`￤`Windows Forms`￤`Microsoft Visual C# 2015`￤`SQL Server 2022`

Desarrollo de mini aplicación para aprender los conceptos básicos del lenguaje C#.

## 💪 Ejercicio

Diseñe el diagrama de modelo relacional de un sistema para registrar las farmacias en diferentes ciudades de nuestro país. Sabemos que cada farmacia tiene un nombre (único en todo el sistema) y un domicilio. Cada farmacia se ubica en una sola ciudad, pero en una ciudad hay varias farmacias. De cada ciudad, sabemos el nombre, la provincia en la que se encuentra, la cantidad de habitantes y la superficie. Cada ciudad se identifica con el nombre y la provincia.<br>
Conocemos también que cada farmacia puede tener un propietario, y que cada propietario tiene solamente una farmacia. Tenga en cuenta que puede haber farmacias sin propietario. De los propietarios, conocemos el DNI (único), su nombre y domicilio, compuesto por calle, número, códigopostal y ciudad.<br>
Cada farmacia, a su vez, vende varios medicamentos y un medicamento se vende en varias farmacias. De cada medicamento conocemos su id único, su nombre comercial y las drogas de las cuales se compone.<br>
Cada Farmacia vende un medicamento a un precio determinado, que no necesariamente es el mismo en diferentes farmacias.

## 🧠 Solución

La propuesta de solución, en lo que respecta al modelo, se encuentra detallada en la carpeta `db` (_[Enlace a la solución](db/readme.md)_).
