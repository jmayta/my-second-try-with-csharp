﻿USE master
GO

DROP DATABASE IF EXISTS FarmaciaTest;
GO

CREATE DATABASE FarmaciaTest;
USE FarmaciaTest;
GO

CREATE SCHEMA MSTWCS;
GO

-- CREACION DE TABLAS

CREATE TABLE MSTWCS.Ciudades
(
  nombre VARCHAR(100) NOT NULL,
  provincia VARCHAR(100) NOT NULL,
  habitantes INT,
  superficie FLOAT,
  CONSTRAINT pk_ciudad PRIMARY KEY (nombre, provincia)
)
GO

CREATE TABLE MSTWCS.Propietarios
(
  dni VARCHAR(8) NOT NULL PRIMARY KEY,
  nombre VARCHAR(100),
  domicilio VARCHAR(254)
  -- Domicilio = Calle + nro + cod. postal + ciudad
)
GO

CREATE TABLE MSTWCS.Medicamentos
(
  id VARCHAR(6) NOT NULL PRIMARY KEY,
  nombre_comercial VARCHAR(254),
  composicion TEXT
)
GO

CREATE TABLE MSTWCS.Farmacias
(
  nombre VARCHAR(254) NOT NULL PRIMARY KEY,
  domicilio VARCHAR(254),
  ciudad_nombre VARCHAR(100),
  ciudad_provincia VARCHAR(100),
  propietario_id VARCHAR(8),
  -- Constraint para ciudad(nombre, provincia)
  CONSTRAINT fk_ciudad_nombre_provincia
	FOREIGN KEY(ciudad_nombre, ciudad_provincia)
	REFERENCES MSTWCS.Ciudades(nombre, provincia),
  -- Constraint para propietario
  CONSTRAINT fk_propietario
	FOREIGN KEY (propietario_id) REFERENCES MSTWCS.Propietarios(dni)
	ON DELETE SET NULL
)
GO

CREATE TABLE MSTWCS.Stock
(
  farmacia_id VARCHAR(254)
    FOREIGN KEY REFERENCES MSTWCS.Farmacias(nombre)
		ON DELETE CASCADE,
  medicamento_id VARCHAR(6)
    FOREIGN KEY REFERENCES MSTWCS.Medicamentos(id)
		ON DELETE CASCADE,
  precio DECIMAL(13,2),
  -- Constraint para no repetir la combinación (farmacia, medicamento)
  CONSTRAINT uc_farmacia_medicamento
	UNIQUE (farmacia_id, medicamento_id)
)
GO


-- STORED PROCEDURES

-- CIUDAD

-- Create

ALTER PROC RegistrarCiudad
    @nombre VARCHAR(100),
    @provincia VARCHAR(100),
    @habitantes INT,
    @superficie FLOAT,
    @message VARCHAR(254) OUTPUT 
AS
BEGIN
  INSERT INTO MSTWCS.Ciudades
    (nombre, provincia, habitantes, superficie)
  VALUES
    (@nombre, @provincia, @habitantes,ROUND(@superficie, 2))
  -- Set success message
  SET @message = 'Se registró la ciudad: ' + CONCAT_WS(', ', @nombre, @provincia)
END
GO

-- Read

CREATE PROC ListarCiudades
AS
SELECT nombre, provincia, habitantes, superficie
FROM MSTWCS.Ciudades
ORDER BY nombre, provincia ASC
GO

CREATE PROC BuscarCiudad
  @querystring VARCHAR(100)
AS
	SELECT nombre, provincia, habitantes, superficie
		FROM MSTWCS.Ciudades
	WHERE nombre LIKE '%' + @querystring + '%' OR provincia  LIKE '%' + @querystring + '%'
	ORDER BY nombre, provincia ASC
GO

CREATE PROC ListarFarmaciasEnCiudad
  @ciudad_nombre VARCHAR(100),
  @ciudad_provincia VARCHAR(100)
AS
SELECT f.nombre, f.domicilio, p.nombre
FROM MSTWCS.Farmacias f
  INNER JOIN MSTWCS.Ciudades c
  ON c.nombre = f.ciudad_nombre AND c.provincia = f.ciudad_provincia
  INNER JOIN MSTWCS.Propietarios p
  ON p.dni = f.propietario_id
WHERE f.ciudad_nombre = @ciudad_nombre AND f.ciudad_provincia = @ciudad_provincia
ORDER BY f.nombre ASC
GO

-- Update
CREATE PROC ModificarCiudad
  @oldNombre VARCHAR(100),
  @oldProvincia VARCHAR(100),
  @nombre VARCHAR(100),
  @provincia VARCHAR(100),
  @habitantes INT,
  @superficie FLOAT,
  @message VARCHAR(254) = '' OUTPUT
AS
BEGIN
	UPDATE MSTWCS.Ciudades
		SET nombre = @nombre, provincia = @provincia, habitantes = @habitantes, superficie = @superficie
	WHERE nombre = @oldNombre AND provincia = @oldProvincia
	IF @@ROWCOUNT > 0
		SET @message = 'Se actualizó satisfactoriamente la ciudad: ' + CONCAT_WS(', ',@oldNombre, @oldProvincia)
END
GO

-- Delete
CREATE PROC EliminarCiudad
  @nombre VARCHAR(100),
  @provincia VARCHAR(100)
AS
DELETE FROM MSTWCS.Ciudades
	WHERE nombre = @nombre AND provincia =  @provincia;
GO


-- PROPIETARIO

-- Create
CREATE PROC RegistrarPropietario
  @dni VARCHAR(8),
  @nombre VARCHAR(100),
  @domicilio VARCHAR(254),
  @message VARCHAR(254) OUTPUT
AS
BEGIN
	INSERT INTO MSTWCS.Propietarios
	  (dni, nombre, domicilio)
	VALUES
	  (@dni, @nombre, @domicilio)
	SET @message = 'Propietario registrado: ' + @nombre
END
GO

-- Read
CREATE PROC ListarPropietarios
AS
SELECT dni, nombre, domicilio
FROM MSTWCS.Propietarios
ORDER BY nombre ASC
GO

CREATE PROC BuscarPropietario
  @queryestring VARCHAR(254)
AS
SELECT dni, nombre, domicilio
FROM MSTWCS.Propietarios
WHERE dni LIKE '%' + @queryestring + '%' OR nombre LIKE '%' + @queryestring + '%'
ORDER BY nombre ASC
GO

CREATE PROC ObtenerFarmaciaDePropietario
  @dni VARCHAR(8)
AS
SELECT f.nombre, f.domicilio, c.nombre, c.provincia
FROM MSTWCS.Farmacias f
  INNER JOIN MSTWCS.Ciudades c
  ON c.nombre = f.ciudad_nombre AND c.provincia = f.ciudad_provincia
WHERE f.propietario_id = @dni
GO

-- Update 
CREATE PROC ModificarPropietario
  @oldDni VARCHAR(8),
  @dni VARCHAR(8),
  @nombre VARCHAR(100),
  @domicilio VARCHAR(254)
AS
UPDATE MSTWCS.Propietarios
	SET dni = @dni, nombre = @nombre, domicilio = @domicilio
	WHERE dni = @oldDni
GO

-- Delete
CREATE PROC EliminarPropietario
  @dni VARCHAR(8)
AS
DELETE MSTWCS.Propietarios
	WHERE dni = @dni
GO

-- MEDICAMENTOS

-- Create
CREATE PROC RegistrarMedicamento
  @id VARCHAR(8),
  @nombreComercial VARCHAR(254),
  @composicion TEXT
AS
INSERT INTO MSTWCS.Medicamentos
  (id, nombre_comercial, composicion)
VALUES
  (@id, @nombreComercial, @composicion)
GO

/*
INSERT INTO Medicamentos
VALUES
  ('001254', 'Aspirina', 'Paracetamol'),
  ('000821', 'Vick Vaporub', 'Pasta de cacao, mentol, alcanfor'),
  ('000825', 'Mentolatum', 'Pasta de cacao, mentol, alcanfor'),
  ('105228', 'Ferranim', 'Hierro'),
  ('205528', 'Dulces Sueños', 'Diazepan')
*/

-- Read
CREATE PROC ListarMedicamentos
AS
SELECT id, nombre_comercial, composicion
FROM MSTWCS.Medicamentos
ORDER BY nombre_comercial ASC
GO

CREATE PROC BuscarMedicamento
  @querystring VARCHAR(254)
AS
SELECT id, nombre_comercial, composicion
FROM MSTWCS.Medicamentos
WHERE nombre_comercial LIKE '%' + @querystring + '%' OR composicion LIKE '%' + @querystring + '%'
ORDER BY nombre_comercial ASC
GO


CREATE PROC BuscarFarmaciasConMedicamento
  @nombreMedicamento VARCHAR(254)
AS
SELECT f.nombre, f.domicilio, c.nombre, c.provincia, s.precio
FROM MSTWCS.Stock s
  INNER JOIN MSTWCS.Medicamentos m ON m.id = s.medicamento_id
  INNER JOIN MSTWCS.Farmacias f ON f.nombre = s.farmacia_id
  INNER JOIN MSTWCS.Ciudades c ON f.ciudad_nombre = c.nombre AND f.ciudad_provincia = c.provincia
WHERE m.nombre_comercial LIKE '%' + @nombreMedicamento + '%'
ORDER BY nombre_comercial, f.nombre ASC
GO

-- Update
CREATE PROC ModificarMedicamento
  @oldId VARCHAR(8),
  @id VARCHAR(8),
  @nombreComercial VARCHAR(254),
  @composicion TEXT
AS
UPDATE MSTWCS.Medicamentos
	SET id = @id, nombre_comercial = @nombreComercial, composicion = @composicion
	WHERE id = @oldId
GO

-- Delete
CREATE PROC EliminarMedicamento
  @id VARCHAR(8)
AS
DELETE MSTWCS.Medicamentos
	WHERE id = @id
GO


-- FARMACIA

-- Create
CREATE PROC RegistrarFarmacia
  @nombre VARCHAR(254),
  @domicilio VARCHAR(254),
  @ciudadNombre VARCHAR(100),
  @ciudadProvincia VARCHAR(100),
  @propietarioId VARCHAR(8)
AS
INSERT INTO MSTWCS.Farmacias
  (nombre, domicilio, ciudad_nombre, ciudad_provincia, propietario_id)
VALUES
  (@nombre, @domicilio, @ciudadNombre, @ciudadProvincia, @propietarioId)
GO

/*
-- test
EXEC sp_registrar_farmacia 'Inkafarma', 'Calle Real 1854', 'Londres', 'Inglaterra', '26584841'
EXEC sp_registrar_farmacia 'FarmaBen', 'Av. Mariscal Castilla 3854', 'Sydney', 'Australia', '47856120'
EXEC sp_registrar_farmacia 'YuviFarma', 'Calle Real 1854', 'Londres', 'Inglaterra', NULL
EXEC sp_registrar_farmacia 'Boticas Arcangel', 'Calle Real 1854', 'Londres', 'Inglaterra', NULL
	GO
*/
-- Read
CREATE PROC ListarFarmacias
AS
SELECT f.nombre, f.domicilio, c.nombre, c.provincia
FROM MSTWCS.Farmacias f
  INNER JOIN MSTWCS.Ciudades c ON c.nombre = f.ciudad_nombre AND c.provincia = f.ciudad_provincia
ORDER BY f.nombre ASC
GO

CREATE PROC ListarMedicamentosEnFarmacia
  @farmacia_id VARCHAR(254)
AS
SELECT m.id, m.nombre_comercial, s.precio
FROM MSTWCS.Stock s
  INNER JOIN MSTWCS.Farmacias f ON f.nombre = s.farmacia_id
  INNER JOIN MSTWCS.Medicamentos m ON m.id = s.medicamento_id
WHERE f.nombre = @farmacia_id
ORDER BY m.nombre_comercial ASC
GO

-- Update
CREATE PROC ModificarFarmacia
  @oldNombre VARCHAR(254),
  @nombre VARCHAR(254),
  @domicilio VARCHAR(254),
  @ciudadNombre VARCHAR(100),
  @ciudadProvincia VARCHAR(100),
  @propietarioId VARCHAR(8)
AS
UPDATE MSTWCS.Farmacias
	SET 
		nombre = @nombre,
		domicilio = @domicilio,
		ciudad_nombre = @ciudadNombre,
		ciudad_provincia = @ciudadProvincia,
		propietario_id = @propietarioId
	WHERE nombre = @oldNombre
GO

-- Delete
CREATE PROC EliminarFarmacia
  @farmaciaId VARCHAR(254)
AS
DELETE MSTWCS.Farmacias
	WHERE nombre = @farmaciaId
GO


-- STOCK

-- Create
CREATE PROC RegistrarStock
  @farmaciaId VARCHAR(254),
  @medicamentoId VARCHAR(6),
  @precio DECIMAL(13,2) = 0.00
AS
INSERT INTO MSTWCS.Stock
  (farmacia_id, medicamento_id, precio)
VALUES
  (@farmaciaId, @medicamentoId, ROUND(@precio,2))
GO

-- UPDATE
CREATE PROC ActualizarStock
  @farmaciaId VARCHAR(254),
  @medicamentoId VARCHAR(6),
  @precio DECIMAL(13,2) = 0.00
AS
UPDATE MSTWCS.Stock
	SET precio = @precio
	WHERE farmacia_id = @farmaciaId AND medicamento_id = @medicamentoId
GO

-- Delete
CREATE PROC EliminarStock
  @farmaciaId VARCHAR(254),
  @medicamentoId VARCHAR(6)
AS
DELETE MSTWCS.Stock
	WHERE farmacia_id = @farmaciaId AND medicamento_id = @medicamentoId
GO