# 🧠 Solución

## Código

El modelo relacional propuesto es el siguiente:

```
// código escrito en lenguaje dbml

Table Ciudades {
  nombre varchar(100) [not null]
  provincia varchar(100) [not null]
  cant_habitantes int
  superficie float
  indexes {
    (nombre, provincia) [pk]
  }
}

Table Farmacias {
  nombre varchar(254) [pk, unique]
  domicilio varchar(254)
  ciudad_nombre varchar(100)
  ciudad_provincia varchar(100)
  propietario_id varchar(8) [unique, null]
}

Table Medicamentos {
  id varchar(6) [pk]
  nombre_comercial varchar(254)
  composicion text
}

Table Propietarios {
  dni varchar(8) [unique, PK]
  nombre varchar(100)
  domicilio varchar(254) [note: 'calle/numero/cpostal/ciudad']
}

Table Stock [headercolor: #8F8DD8] {
  farmacia_id varchar(254)
  medicamento_id int
  precio decimal(13,2) [default: '0.00']
  indexes {
    (farmacia_id, medicamento_id) [unique]
  }
}

Ref: Ciudades.nombre < Farmacias.ciudad_nombre
Ref: Ciudades.provincia < Farmacias.ciudad_provincia
Ref: Farmacias.propietario_id - Propietarios.dni
Ref: Stock.farmacia_id > Farmacias.nombre
Ref: Stock.medicamento_id > Medicamentos.id
```

## Diagrama

Diagrama correspondiente al código anterior.

![Diseño relacional](farmacia.png)
