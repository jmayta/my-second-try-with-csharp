﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using ENL;

namespace BLL
{
    public class BLL_Ciudad
    {
        private readonly DAL_Ciudad dalCiudad = new DAL_Ciudad();
        public DataTable GetCiudades()
        {
            return dalCiudad.GetCiudades();
        }

        public DataTable GetCiudad(string queryString)
        {
            var dtCiudades = GetCiudades()
                .AsEnumerable()
                .Where(x => x.Field<string>("nombre").IndexOf(queryString, StringComparison.OrdinalIgnoreCase) >= 0);

            DataTable ciudades = new DataTable();
            try
            {
                ciudades = dtCiudades.CopyToDataTable();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return ciudades;
        }

        public string InsertCiudad(ENL_Ciudad Ciudad)
        {
            return dalCiudad.InsertCiudad(Ciudad);
        }

        public string UpdateCiudad(ENL_Ciudad ciudad)
        {
            return dalCiudad.UpdateCiudad(ciudad);
        }

        public string DeleteCiudad(ENL_Ciudad ciudad)
        {
            return dalCiudad.DeleteCiudad(ciudad);
        }

    }
}
