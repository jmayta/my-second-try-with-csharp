﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using ENL;

namespace UIL
{
    public partial class frmCiudad : Form
    {
        #region Declaraciones
        private readonly BLL_Ciudad bllCiudad = new BLL_Ciudad();
        ENL_Ciudad objCiudad = new ENL_Ciudad();
        internal enum Estado
        {
            Lectura,
            Registro,
            Edicion
        }
        private Estado _estadoActual;
        internal Estado EstadoActual
        {
            get
            {
                return _estadoActual;
            }

            set 
            {
                _estadoActual = value;
                switch (_estadoActual)
                {
                    case Estado.Lectura:
                        txtBuscarCiudad.Enabled = true;
                        txtNombre.Enabled = false;
                        txtProvincia.Enabled = false;
                        txtHabitantes.Enabled = false;
                        txtSuperficie.Enabled = false;
                        tsmiGuardar.Enabled = false;
                        tsmiCancelar.Enabled = false;
                        tsmiNuevo.Enabled = true;
                        tsmiActualizar.Enabled = true;
                        tsmiEliminar.Enabled = true;
                        dgvCiudades.Enabled = true;
                        break;
                    case Estado.Registro:
                        Limpiar();
                        txtBuscarCiudad.Enabled = false;
                        txtNombre.Enabled = true;
                        txtProvincia.Enabled = true;
                        txtHabitantes.Enabled = true;
                        txtSuperficie.Enabled = true;
                        tsmiGuardar.Enabled = true;
                        tsmiCancelar.Enabled = true;
                        tsmiNuevo.Enabled = false;
                        tsmiActualizar.Enabled = false;
                        tsmiEliminar.Enabled = false;
                        dgvCiudades.Enabled = false;
                        break;
                    case Estado.Edicion:
                        txtBuscarCiudad.Enabled = false;
                        txtNombre.Enabled = true;
                        txtProvincia.Enabled = true;
                        txtHabitantes.Enabled = true;
                        txtSuperficie.Enabled = true;
                        tsmiGuardar.Enabled = true;
                        tsmiCancelar.Enabled = true;
                        tsmiNuevo.Enabled = false;
                        tsmiActualizar.Enabled = false;
                        tsmiEliminar.Enabled = false;
                        dgvCiudades.Enabled = false;
                        break;
                }
            }
        }

        private void Limpiar()
        {
            txtNombre.Text = String.Empty;
            txtProvincia.Text = String.Empty;
            txtHabitantes.Text = String.Empty;
            txtSuperficie.Text = String.Empty;
            txtBuscarCiudad.Text = String.Empty;
        }
        #endregion

        public frmCiudad()
        {
            InitializeComponent();
        }

        private void frmCiudad_Load(object sender, EventArgs e)
        {
            EstadoActual = Estado.Lectura;
            dgvCiudades.DataSource = bllCiudad.GetCiudades();
        }

        private void txtBuscarCiudad_TextChanged(object sender, EventArgs e)
        {
            if (txtBuscarCiudad.Text != String.Empty)
            {
                dgvCiudades.DataSource = bllCiudad.GetCiudad(txtBuscarCiudad.Text);
            }
            else
            {
                dgvCiudades.DataSource = bllCiudad.GetCiudades();
            }
        }

        private void dgvCiudades_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtNombre.Text = dgvCiudades.CurrentRow.Cells["nombre"].Value.ToString();
            txtProvincia.Text = dgvCiudades.CurrentRow.Cells["provincia"].Value.ToString();
            txtHabitantes.Text = dgvCiudades.CurrentRow.Cells["habitantes"].Value.ToString();
            txtSuperficie.Text = dgvCiudades.CurrentRow.Cells["superficie"].Value.ToString();
        }

        private void tsmiNuevo_Click(object sender, EventArgs e)
        {
            EstadoActual = Estado.Registro;
        }

        private void tsmiGuardar_Click(object sender, EventArgs e)
        {
            string mensaje = String.Empty;
            objCiudad.Nombre = txtNombre.Text;
            objCiudad.Provincia = txtProvincia.Text;
            objCiudad.Habitantes = Convert.ToInt32(txtHabitantes.Text);
            objCiudad.Superficie = Convert.ToSingle(txtHabitantes.Text);

            if (EstadoActual == Estado.Registro)
            {
                mensaje = bllCiudad.InsertCiudad(objCiudad);
                MessageBox.Show(mensaje, "Registro Exitoso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } 
            else if (EstadoActual == Estado.Edicion)
            {
                mensaje = bllCiudad.UpdateCiudad(objCiudad);
                MessageBox.Show(mensaje, "Modificación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            dgvCiudades.DataSource = bllCiudad.GetCiudades();
            Limpiar();
            EstadoActual = Estado.Lectura;
        }

        private void tsmiActualizar_Click(object sender, EventArgs e)
        {
            EstadoActual = Estado.Edicion;

            objCiudad.OldNombre = txtNombre.Text;
            objCiudad.OldProvincia = txtProvincia.Text;
        }

        private void tsmiCancelar_Click(object sender, EventArgs e)
        {
            EstadoActual = Estado.Lectura;
        }

        private void tsmiEliminar_Click(object sender, EventArgs e)
        {
            string mensaje = String.Empty;
            objCiudad.Nombre = txtNombre.Text;
            objCiudad.Provincia = txtProvincia.Text;

            DialogResult resultado = MessageBox.Show(
                $"Está a punto de eliminar la ciudad {objCiudad.Nombre},{objCiudad.Provincia}", 
                "Confirmar Eliminación", 
                MessageBoxButtons.YesNo, 
                MessageBoxIcon.Warning
            );

            if (resultado == DialogResult.Yes)
            {
                mensaje = bllCiudad.DeleteCiudad(objCiudad);
                MessageBox.Show(mensaje, "Eliminación Exitosa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Limpiar();
                EstadoActual = Estado.Lectura;
            }
        }
    }
}
