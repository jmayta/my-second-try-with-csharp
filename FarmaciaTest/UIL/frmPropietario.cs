﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ENL;
using BLL;

namespace UIL
{
    public partial class frmPropietario : Form
    {


        BLL_Propietario Propietario = new BLL_Propietario();


        public frmPropietario()
        {
            InitializeComponent();
        }

        private void frmPropietario_Load(object sender, EventArgs e)
        {
            dgvPropietarios.DataSource = Propietario.Lista();
        }
    }
}
