﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENL
{
    public class ENL_Farmacia
    {
        public string Nombre { get; set; }
        public string Domicilio { get; set; }
        public string CiudadNombre { get; set; }
        public string CiudadProvincia { get; set; }
        public string PropietarioId { get; set; }
        public ENL_Ciudad Ciudad { get; set; }
        public List<ENL_Stock> Medicamentos { get; set; }
    }
}
