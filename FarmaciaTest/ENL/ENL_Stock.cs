﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENL
{
    public class ENL_Stock
    {
        public string FarmaciaId { get; set; }
        public string MedicamentoId { get; set; }
        public double Precio { get; set; }
        public ENL_Farmacia Farmacia { get; set; }
        public ENL_Medicamento Medicamento { get; set; }
    }
}
