﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENL
{
    public class ENL_Ciudad
    {
        public string Nombre { get; set; }
        public string Provincia { get; set; }
        public int Habitantes { get; set; }
        public float Superficie { get; set; }
        public string OldNombre { get; set; }
        public string OldProvincia { get; set; }
        public List<ENL_Farmacia> Farmacias { get; set; }
    }
}
