﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENL
{
    public class ENL_Medicamento
    {
        public string Id { get; set; }
        public string NombreComercial { get; set; }
        public string Composicion { get; set; }
        public List<ENL_Farmacia> Farmacias { get; set; }
    }
}
