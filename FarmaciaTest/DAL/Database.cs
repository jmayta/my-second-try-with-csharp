﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Database
    {
        private string _connectionString;
        private SqlConnection _connection;
        private SqlCommand _command;


        public string ConnectionString { get => _connectionString; set => _connectionString = value; }
        public SqlConnection Connection { get => _connection; set => _connection = value; }
        public SqlCommand Command { get => _command; set => _command = value; }

        public Database() { }

        public Database(string connectionString, string storedProcedure) : this()
        {
            this.Connection = new SqlConnection(connectionString);
            this.Command = new SqlCommand(storedProcedure, this.Connection);
        }

    }
}
