﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using ENL;

namespace DAL
{
    public class DAL_Propietario
    {
        public DataTable GetPropietarios()
        {
            DataTable tablaPropietario = new DataTable("Propietarios");
            try
            {
                using(SqlConnection connection = new SqlConnection(Conexion.ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("ListarPropietario", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            connection.Open();
                            DataSet ds = new DataSet();
                            adapter.Fill(ds, "Propietarios");
                            tablaPropietario = ds.Tables["Propietarios"];

                            return tablaPropietario;
                        }
                    }
                }
            }
            catch (SqlException sqlex)
            {
                Console.WriteLine($"Error en Data Access Layer (DAL):\n{sqlex.ErrorCode} : {sqlex.Message}");
                return tablaPropietario;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return tablaPropietario;
            }
        }

        public string InsertPropietario(ENL_Propietario propietario)
        {
            return "";
        }
    }
}
