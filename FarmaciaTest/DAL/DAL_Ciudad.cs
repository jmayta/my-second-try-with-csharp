﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ENL;

namespace DAL
{
    public class DAL_Ciudad
    {
        public DataTable GetCiudades()
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Conexion.ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("ListarCiudades", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            using (DataSet ds = new DataSet())
                            {
                                DataTable dt = new DataTable("Ciudades");
                                adapter.Fill(ds, "Ciudades");
                                dt = ds.Tables["Ciudades"];
                                return dt;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string InsertCiudad(ENL_Ciudad ciudad)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Conexion.ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("RegistrarCiudad", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@nombre", SqlDbType.VarChar, 100);
                        command.Parameters["@nombre"].Value = ciudad.Nombre;
                        
                        command.Parameters.Add("@provincia", SqlDbType.VarChar, 100);
                        command.Parameters["@provincia"].Value = ciudad.Provincia;
                        
                        command.Parameters.Add("@habitantes", SqlDbType.Int);
                        command.Parameters["@habitantes"].Value = ciudad.Habitantes;

                        command.Parameters.Add("@superficie", SqlDbType.Float);
                        command.Parameters["@superficie"].Value = ciudad.Superficie;

                        command.Parameters.Add("@message", SqlDbType.VarChar, 254);
                        command.Parameters["@message"].Direction = ParameterDirection.Output;

                        connection.Open();
                        command.ExecuteNonQuery();

                        return command.Parameters["@message"].Value.ToString();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string UpdateCiudad(ENL_Ciudad ciudad)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Conexion.ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("ModificarCiudad", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        command.Parameters.Add("@oldNombre", SqlDbType.VarChar, 100);
                        command.Parameters["@oldNombre"].Value = ciudad.OldNombre;

                        command.Parameters.Add("@oldProvincia", SqlDbType.VarChar, 100);
                        command.Parameters["@oldProvincia"].Value = ciudad.OldProvincia;

                        command.Parameters.Add("@nombre", SqlDbType.VarChar, 100);
                        command.Parameters["@nombre"].Value = ciudad.Nombre;

                        command.Parameters.Add("@provincia", SqlDbType.VarChar, 100);
                        command.Parameters["@provincia"].Value = ciudad.Provincia;

                        command.Parameters.Add("@habitantes", SqlDbType.Int);
                        command.Parameters["@habitantes"].Value = ciudad.Habitantes;

                        command.Parameters.Add("@superficie", SqlDbType.Float);
                        command.Parameters["@superficie"].Value = ciudad.Superficie;

                        command.Parameters.Add("@message", SqlDbType.VarChar, 254);
                        command.Parameters["@message"].Direction = ParameterDirection.Output;

                        connection.Open();
                        command.ExecuteNonQuery();

                        return command.Parameters["@message"].Value.ToString();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string DeleteCiudad(ENL_Ciudad ciudad)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(Conexion.ConnectionString))
                {
                    using (SqlCommand command = new SqlCommand("EliminarCiudad", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@nombre", ciudad.Nombre);
                        command.Parameters.AddWithValue("@provincia", ciudad.Provincia);

                        connection.Open();

                        int rowsAfectadas = command.ExecuteNonQuery();
                        if (rowsAfectadas > 0)
                        {
                            return $"La ciudad {ciudad.Nombre}, {ciudad.Provincia} fue eliminada exitosamente";
                        }
                        else
                        {
                            return $"La ciudad {ciudad.Nombre}, {ciudad.Provincia} no fue encontrada en nuestros registros";
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
