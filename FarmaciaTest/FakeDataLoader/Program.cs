﻿using BLL;
using Bogus;
using ENL;
using System;
using System.Collections.Generic;

namespace FakeDataLoader
{
    internal class Program
    {
        static void Main(string[] args)
        {
            BLL_Ciudad bllCiudad = new BLL_Ciudad();

            Randomizer.Seed = new Random(46594611);
            var testCiudades = new Faker<ENL_Ciudad>()
                // Genera datos aleatorios para cada atributo
                .RuleFor(c => c.Nombre, f => f.Address.City())
                .RuleFor(c => c.Provincia, f => f.Address.State())
                .RuleFor(c => c.Habitantes, f => f.Random.Int(5000, 1000000))
                .RuleFor(c => c.Superficie, f => Convert.ToSingle(f.Random.Decimal() * f.Random.Int(1000, 1000000)));

            

            // Genera una lista de 50 ciudades
            List<ENL_Ciudad> ciudades = testCiudades.Generate(50);
            foreach (ENL_Ciudad ciudad in ciudades)
            {
                string message = bllCiudad.InsertCiudad(ciudad);
                Console.WriteLine($"{message}");
            }
        }

    }
}
